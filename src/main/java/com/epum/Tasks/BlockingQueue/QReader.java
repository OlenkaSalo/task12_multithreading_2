package com.epum.Tasks.BlockingQueue;

import java.util.concurrent.BlockingQueue;

class QReader extends Thread{

     private BlockingQueue<String> bqInput ;

    public QReader(BlockingQueue bqInput) {
        this.bqInput = bqInput;

    }

    @Override
    public void run()
    {
        try {
           for(int i = 0; i<100; i++) {
             System.out.print(bqInput.take() + "" + i);
           }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

